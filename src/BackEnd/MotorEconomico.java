package BackEnd;

public class MotorEconomico extends Motor {

	public MotorEconomico() {
		System.out.println("Creando Motor Econůmico");
	}

	@Override
	public void encender() {
		System.out.println("Encendiendo Motor Econůmico");
	}

	@Override
	public void acelerar() {
		System.out.println("Acelerando Motor Econůmico");
	}

	@Override
	public void apagar() {
		System.out.println("Apagando Motor Econůmico");
	}

}
