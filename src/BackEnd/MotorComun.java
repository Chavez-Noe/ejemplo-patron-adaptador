package BackEnd;

public class MotorComun extends Motor {
	
	public MotorComun() {
		System.out.println("Creando Motor Com�n");
	}

	@Override
	public void encender() {
		System.out.println("Encendiendo Motor Com�n");
	}

	@Override
	public void acelerar() {
		System.out.println("Acelerando Motor Com�n");
	}

	@Override
	public void apagar() {
		System.out.println("Apagando Motor Com�n");
	}

}
