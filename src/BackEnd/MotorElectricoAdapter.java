package BackEnd;

public class MotorElectricoAdapter extends Motor {
	
	private MotorElectrico motorElectrico;
	
	public MotorElectricoAdapter() {
		motorElectrico = new MotorElectrico();
		System.out.println("Creando Motor El�ctrico Adapter");
	}

	@Override
	public void encender() {
		System.out.println("Encendiendo motor electrico adapter");
		motorElectrico.encender();
	}

	@Override
	public void acelerar() {
		motorElectrico.moverMasRapido();
	}

	@Override
	public void apagar() {
		motorElectrico.detener();
		motorElectrico.desconectar();
	} 

}
