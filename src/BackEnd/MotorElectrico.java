package BackEnd;

public class MotorElectrico {
	
	private boolean conectado;  
	
	public MotorElectrico() {
		
		conectado = false;
		System.out.println("Creando motor el�ctrico");
		
	}
	
	public void encender() {
		if (conectado) {
			System.out.println("Desconectar primero");
		} else {
			System.out.println("Encender motor el�ctrico");
		}
	}
	
	public void activarCargador() {
		if (!conectado) {
			System.out.println("No se puede activar porque no est� conectado el motor el�ctrico");
		} else {
			System.out.println("Activando carga de motor");
		}
		
	}
	
	public void moverMasRapido() {
		System.out.println("Aumentando la velocidad");
	}
	
	public void detener() {
		System.out.println("Deteniendo motor electrico");
	}
	
	public void desconectar() {
		System.out.println("Motor desconectado");
		conectado = false;
	}

}
