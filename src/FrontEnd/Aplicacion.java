package FrontEnd;

import java.util.Scanner;

import BackEnd.Motor;
import BackEnd.MotorComun;
import BackEnd.MotorEconomico;
import BackEnd.MotorElectricoAdapter;

public class Aplicacion {
	
	private static Scanner s = new Scanner(System.in);  
	private static Motor motor;

	public static void main(String[] args) {
		
		int opcion;
		
		do {
			
			opcion = preguntarOpcion();
			
			switch (opcion) {
			
			case 1:
				motor = new MotorComun();
				usarMotor();
				break;
				
			case 2: 
				motor = new MotorEconomico();
				usarMotor();
				break;
				
			case 3:
				motor = new MotorElectricoAdapter();
				usarMotor();
				break;
				
			case 4:
				System.out.println("Cerrando programa");

			default:
				System.out.println("Opci�n inv�lida");
				break;
			}
			
		} while (opcion != 4);

	}
	
	private static int preguntarOpcion() {
		
		System.out.println("Men� de opciones\n"
				+ "1. Encender motor com�n.\n"
				+ "2. Encender motor econ�mico\n"
				+ "3. Encender motor el�ctrico\n"
				+ "4. Salir.\n"
				+ "Seleciones una opci�n: ");
		
		return Integer.parseInt(s.nextLine());
	}
	
	private static void usarMotor() {
		motor.encender();
		motor.acelerar();
		motor.apagar();
	}

}
